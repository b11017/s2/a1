alert("Hello World")

let number = Number(prompt("Pick a number: "))
console.log("The number you provided is " + number);

for(number; number !== 0; number--){
	
	if(number % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	} else if(number % 5 === 0){
		console.log(number + "")
	
	} else if(number <= 50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	}
};


let name = "supercalifragilisticexpialidocious.";

console.log(name);

for(let i = 0; i < name.length; i++){
	
	if(
		name[i].toLowerCase() == "a" ||
		name[i].toLowerCase() == "e" ||
		name[i].toLowerCase() == "i" ||
		name[i].toLowerCase() == "o" ||
		name[i].toLowerCase() == "u"
	){

		console.log("Continue to next iteration")
	
	} 

};





